# Linux uPlatform Development Meeting

Kickoff 12/4 3-5pm CET

*Zoom Info:*
Join Zoom Meeting
https://eclipse.zoom.us/j/85386857622?pwd=1vzugiHG3DXJqmbqsodYWJAialEPns.1

Meeting ID: 853 8685 7622
Passcode: 947209

## Purpose
Joint initiative with the community to build a reference Linux uPlatform *uprotocol-platform-linux* based off the Zenoh communication protocol and rust programming language. uPlatforms contains the common required core uServices uSubscription, uDiscovery, uTwin, and the device-2-device (or platform-2-platform) event dispatcher entity called uStreamer.


The platform will require the following core libraries for development:
- *uprotocol-core-api:* Define the common Data Model
- *uprotocol-rust:* Language specific library per uprotocol-specs that extends the datamodel and provides builders, validators, serializers, etc...
- *uprotocol-rust-ulink-zenoh*: uLink library that implements uTransport and RpcClient APIs that provides the common communication library for building the uPlatform (i.e. set of services and dispatchers)

NOTE: progress/status of the above libraries will be covered by the uProtocol-Zenoh weekly meeting

TODO: Add phases and schedule for this platform (ex. local communication then android-2-linux, etc...)


## Meeting Agenda 12/4/2023

- Introductions & Meeting best Practices
- SoW discussions: Linux (Zenoh transport based) uPlatform

| uPlatform Components | Purpose | Effort |
| --------- | ------- | ------ |
| **uSubscription** | Enable subscription management | |
| **uDiscovery** | So uEs can find/connect to each other||
| **uTwin** | Cache to store published events that flow through a given device||
| **uStreamer** | Connect to the Android & Azure uPlatforms||

Complete Story (Blueprint): 
- Hello-world app & service (defined in COVESA)
- Sample uServices (using Kuksa?)
 





