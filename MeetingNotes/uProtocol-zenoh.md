# uProtocol & Zenoh Meeting

Weekly on Wednesdays 2pm CET

*Zoom Info:*
Join Zoom Meeting
https://eclipse.zoom.us/j/83187682865?pwd=CAxhUyIRY0PSXqcVw2XYuRxtvN1GVL.1

Meeting ID: 831 8768 2865
Passcode: 497074

## Purpose
The purpose of this weekly sync up meeting is the following:
- Define extensions to zenoh to support uProtocol attributes
- Status & updates for dev of the various uprotocol-[java|cpp|rust|python]-ulink-zenoh libraries
- Automotive ASIL-B requirements, safety, security, authorization & identity (when using zenoh within the vehicle)
- Shared memory support (ability to hook in vendor specific solutions to uTransport/zenoh)

----------------

## Meeting Agenda 12/6/2023

- **(Steven)** Introductions & Meeting best Practices
- **(Luca)** Status of zenoh header extensions
- **(Luca)** conan package support for zenoh-c per https://github.com/conan-io/conan-center-index/tree/master/docs/adding_packages 
  - To setup build recipe for clean library dependency management for C++
- uLink Library Status:
    - **(Michael)** C++
    - **(CK)** Java & Rust
    - **(Neelam)** Python
- **(Steven)** Hiccups with using Zenoh in uProtocol (See below)

### Zenoh & uProtocol Hiccup

With uProtocol 1.5 we've defined interfaces for uTransport, RpcClient with a clean & clear data model now defined in protos (UUri, UAttributes, UMessage) in uprotocol-core-api. We have also specified in 1.5 that the uLink library implements uTransport and RpcClient such that it can "connect" the uEs to the platform as seen in the figure below. Both the Android & Cloud uPlatforms fit well into this mental model as the dispatchers and the subscription management, are separate from the transport technology, (i.e. binder transport for Android and EventHub transport for Azure, etc...). It was simple to build centralized uSubscription service and controls the "fanning out" of published events based on permissions (i.e. CAP).   

![uProtocol](uprotocol.png)

With the ongoing development of zenoh based utransports (and uPlatforms), we've proved that it is very simple to implement the uTransport APIs using zenoh session, subscriber, and publisher interfaces HOWEVER the simple mental model (defined above), breaks down when we involve the core uEs. Unlike Android and Cloud transports and platforms, the zenoh "network" is a decentralized peer-2-peer network where subscription management and event dispatching is done by the uEs themselves and not through any centralized services (uSubscription service, dispatcher service, etc...). The de-centralized pub/sub architecture means that we cannot delegate permission management to some central authority (i.e. dispatchers or uSubscription service) and it means we cannot bridge subscriptions across transports/platforms (i.e. outsize of the zenoh world), the key reason for uProtocol!

![Zenoh Modes](zenoh-mode.png)

Given the above mentioned problems identified with zenoh integration (or any other distributed P2P transport protocols to that matter), there are a few solutions to be discussed:

**1. Client Mode:** All communication flows through a zenoh Router (i.e how we treated Binder, HTTP, MQTT, etc..)
- Client-side library (uLink) 
  - Zenoh ulink implements uTransport API (and wraps publisher & subscriber APIs)
  - client-side uSubscription simply does queriables to zenoh plugin
- uStreamer (Zenoh Router):
  - Implements uSubscription, uTwin service
  - CAP (authorization) for both queriables & subscription management

**2. Peer Mode:**
- Client-side library (uLink)
  - Implements RpcClient (wrapper for zenoh queriables) to supports invoking methods defined for uServices (uDiscovery)
  - Implements core uServices (uSubscription & uTwin)
  - Zenoh MUST implement CAP for queriables and for subscribing to topics (when we Session::declare_subscriber)
  - Service/producer performs CAP, requiring passing context (that is identifiable) 
  - There is no "uTransport"
- uStreamer (a.k.a Zenoh-Router):
  - Implement remote uSubscription logic
  - Bridge to other uTransports (SOME/IP, MQTT, etc....) 

**3. Other??**

**Notes from Meeting:**
  - Capturing the requirements for the authorization and privacy issues regarding the uSubscription issue presented by Steven. Present requirements at the next meeting.
  - Present initial ideas (directions) to answer the above.
  - The requirements should take into account mixed scenario with e.g. Zenoh and MQTT in the same system.
  - E2E (per-message) mechanisms may be investigated.
