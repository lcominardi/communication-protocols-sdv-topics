# Sommr-uProtocol Integration Meetings

Kickoff 12/6 3-4pm CET

*Zoom Info:*
Join Zoom Meeting
https://eclipse.zoom.us/j/88274093245?pwd=8wJWNL57bnzs5foAFUB3qulNfhGaf4.1

Meeting ID: 882 7409 3245
Passcode: 224371

## Purpose
Work together as a community to build a sommr SOME/IP-based uProtocol transport. The meetings will be to cover:
- Defining the uprotocol-spec uTransport specifications [sommr.adoc](https://github.com/eclipse-uprotocol/uprotocol-spec/blob/main/up-l1/sommr.adoc) to map UAttributes, uPayload, UUri to SOME/IP and sommr implementation.  (old WiP to be shared)
- Discuss languages, timeline, SoW
- Features/functionality required (i.e. SecOC, SOME/IP-SD, serialization formats, etc...)
- timelines, deliverables, etc... 


## Meeting Agenda 12/6/2023

- Introductions & Meeting best Practices
- Kick off discussions
- sommr Open Source Status
  - December closing AUTOSAR IP reviews
  - January/February Target for approval and release
  - sommr is daemon is written in rust with C++ & Rust bindings
  - Runs on SoCs (in the high-compute space) and is fully tested and validated against AUTOSAR classic SOME/IP stacks  
- Action Items: 
  - **Tom:** 
    - Releasing documentation for sommr
    - List of features/functionality (what is and isn't supported) if not in documentation
    - check in what is needed for ASIL-B certification of Rust compiler
  - **Daniel:**  
    - Could Eclipse SDV working group allocate any support (i.e. like ThreadX) for certifications of joint eclipse SDV projects?
  - **Steven:** 
    - Architecture diagram for SOME/IP bridge (for Android & Linux uPlatforms)
    - SOME/IP serialization/deserializartion extensions for protobuf
    - Next Meeting 12/20


